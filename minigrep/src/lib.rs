//! # minigrep
//!
//! `minigrep` search for matches of a string in a file.
use std::{env, error::Error, fs};

pub struct Config {
    pub query: String,
    pub filename: String,
    pub ignorecase: bool,
}

impl Config {
    pub fn new(args: &[&str]) -> Result<Config, &'static str> {
        let mut args = args.iter();

        let query = match args.next() {
            Some(arg) => arg.to_string(),
            None => return Err("No query string included"),
        };
        let filename = match args.next() {
            Some(arg) => arg.to_string(),
            None => return Err("No filename included"),
        };

        // Explicitly check for "true"/"false" values:
        //let ignorecase: bool = match env::var("IGNORECASE")
        //.unwrap_or_else(|_| "false".to_owned())
        //.parse()
        //{
        //Ok(s) => s,
        //_ => false,
        //};
        // Only check whether it is set or not:
        let ignorecase: bool = !env::var("IGNORECASE").is_err();

        Ok(Config {
            query,
            filename,
            ignorecase,
        })
    }
}

/// Run program with the given Config.
///
/// # Errors
///
/// Will fail if the provided `config.filename` doesn't exist or is not
/// readable
pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(&config.filename)?;

    let matches = search(&config.query, &contents, config.ignorecase);

    if matches.len() == 0 {
        println!("No matches found.");
    } else {
        for (index, line) in matches {
            println!("{}: {}", index, line);
        }
    }

    Ok(())
}

/// Search for lines containing a string.
///
/// # Examples
///
/// ```
/// use minigrep::search;
///
/// let result = search("hello", "hello dolly\nbye", false);
/// assert_eq!(result, vec![(1, "hello dolly")]);
/// ```
pub fn search<'a>(query: &str, contents: &'a str, ignorecase: bool) -> Vec<(usize, &'a str)> {
    // version using loop:
    //let mut results = Vec::new();
    //for (index, line) in contents.lines().enumerate() {
    //  if !ignorecase && line.contains(query)
    //    || ignorecase && line.to_lowercase().contains(&query.to_lowercase())
    //  {
    //    results.push((index + 1, line));
    //  }
    //}

    // version using iterator and "functional" approach (longer but clearer?)
    contents
        .lines()
        .enumerate()
        .filter(|(_, line)| {
            if ignorecase {
                line.to_lowercase().contains(&query.to_lowercase())
            } else {
                line.contains(query)
            }
        })
        // we want lines to start with 1, not 0
        .map(|(index, line)| (index + 1, line))
        .collect()
}

#[cfg(test)]
mod test {
    use super::*;

    // TODO: figure out how to generate correct args for testing
    #[test]
    fn parse_ok() {
        let config = Config::new(&["hello", "file.txt"]).unwrap();
        assert_eq!(config.query, "hello");
        assert_eq!(config.filename, "file.txt");
    }

    #[test]
    #[should_panic(expected = "No query string")]
    fn parse_no_args() {
        let args = [];
        Config::new(&args).unwrap();
    }

    #[test]
    #[should_panic(expected = "No filename")]
    fn parse_too_few_args() {
        let args = ["hello"];
        Config::new(&args).unwrap();
    }

    #[test]
    fn one_result() {
        let query = "duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.";

        assert_eq!(
            vec![(2, "safe, fast, productive.")],
            search(query, contents, false)
        );
    }

    #[test]
    fn result_ignorecase() {
        let query = "DUCT";
        let contents = "\
Rust DucT:
safe, fast, productive.
Pick three.";

        assert_eq!(
            vec![(1, "Rust DucT:"), (2, "safe, fast, productive.")],
            search(query, contents, true)
        );
    }

    #[test]
    fn many_results() {
        let query = "st";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.";

        assert_eq!(
            vec![(1, "Rust:"), (2, "safe, fast, productive.")],
            search(query, contents, false)
        );
    }

    #[test]
    fn no_results() {
        let empty: Vec<(usize, &str)> = vec![];
        let query = "Foost";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.";

        assert_eq!(empty, search(query, contents, false));
    }
}
