use std::env;
use std::process;

use minigrep::Config;

fn main() {
    // For testability, we don't want Config::new to depend on `Args`.
    // Collect args into a vector:
    let args: Vec<String> = env::args().skip(1).collect();
    // Convert the vector of String into a vector of &str:
    let vargs: Vec<&str> = args.iter().map(|s|{&s[..]}).collect();

    let config = Config::new(&mut &vargs).unwrap_or_else(|err| {
        eprintln!("{}", err);
        process::exit(1);
    });

    println!("Searching for '{}' in file {}, ignorecase={}:", config.query, config.filename, config.ignorecase);
    if let Err(e) = minigrep::run(config) {
        eprintln!("Application error: {}", e);
        process::exit(1);
    }
}
