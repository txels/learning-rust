use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::fmt::Debug;

fn main() {
    let answer = 42;
    let message = "hello";
    let float = 2.7212;

    let display: Vec<&dyn Debug> = vec![&message, &answer, &float];

    for d in display {
        println!("got {:?}", d);
    }

    //
    let mut m = HashMap::new();
    m.insert("one", 1);
    m.insert("two", 2);

    if let Some(r) = m.get_mut("three") {
        *r = 10;
    } else {
        m.insert("three", 3); // can't borrow mutably again!
    }

    // entry API
    match m.entry("one") {
        Entry::Occupied(e) => {
            *e.into_mut() = 10;
        }
        Entry::Vacant(e) => {
            e.insert(1);
        }
    };
    println!("{:?}", m);

    // split
    if let Some((first, last)) = "helló=dolly".split_once('=') {
        println!("{:?}", (first, last));
    }
}
