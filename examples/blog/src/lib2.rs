pub struct Post {
    content: String,
}

impl Post {
    pub fn new() -> DraftPost {
        DraftPost {
            content: String::new(),
        }
    }

    pub fn content<'a>(&'a self) -> &'a str {
        &self.content
    }
}

pub struct DraftPost {
    content: String,
}

impl DraftPost {
    pub fn add_text(mut self, text: &str) -> DraftPost {
        self.content.push_str(text);
        self
    }

    pub fn request_review(self) -> PendingReviewPost {
        PendingReviewPost {
            content: self.content,
        }
    }

    fn preview<'a>(&'a self) -> &'a str {
        &self.content
    }
}

pub struct PendingReviewPost {
    content: String,
}

impl PendingReviewPost {
    pub fn approve(self) -> Post {
        Post {
            content: self.content,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn post_factory() -> DraftPost {
        let mut post = Post::new();
        post.add_text("I ate a salad for lunch today");
        post
    }

    #[test]
    fn post_may_add_text_to_draft() {
        let mut post = post_factory();

        post.add_text(", really.");

        assert_eq!("I ate a salad for lunch today, really.", post.preview());
    }

    #[test]
    fn post_may_request_review() {
        let post = post_factory();

        post.request_review();
    }

    #[test]
    fn post_approval_ok() {
        let post = post_factory().request_review();

        let post = post.approve();

        assert_eq!("I ate a salad for lunch today", post.content());
    }
}
