pub struct Post {
    state: Option<Box<dyn State>>,
    content: String,
}

impl Post {
    pub fn new() -> Post {
        Post {
            state: Some(Box::new(Draft {})),
            content: String::new(),
        }
    }

    pub fn add_text(&mut self, text: &str) {
        if self.state.as_ref().unwrap().editable() {
            self.content.push_str(text);
        }
    }

    pub fn request_review(&mut self) {
        if let Some(s) = self.state.take() {
            self.state = Some(s.request_review())
        }
    }

    pub fn approve(&mut self) {
        if let Some(s) = self.state.take() {
            self.state = Some(s.approve())
        }
    }

    pub fn content(&self) -> &str {
        self.state.as_ref().unwrap().content(self)
    }

    pub fn preview(&self) -> &str {
        &self.content
    }
}

trait State {
    fn editable(&self) -> bool {
        false
    }
    fn request_review(self: Box<Self>) -> Box<dyn State>;
    fn approve(self: Box<Self>) -> Box<dyn State>;
    fn content<'a>(&self, _: &'a Post) -> &'a str {
        ""
    }
}

struct Draft {}

impl State for Draft {
    fn editable(&self) -> bool {
        true
    }

    fn request_review(self: Box<Self>) -> Box<dyn State> {
        Box::new(PendingReview::new())
    }

    fn approve(self: Box<Self>) -> Box<dyn State> {
        //panic!("Draft can't be approved");
        self
    }
}

struct PendingReview {
    approvals: u32,
}

impl PendingReview {
    fn new() -> Self {
        PendingReview{approvals: 0}
    }
}

impl State for PendingReview {
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn approve(mut self: Box<Self>) -> Box<dyn State> {
        self.approvals += 1;
        if self.approvals == 2 {
            Box::new(Approved {})
        } else {
            self
        }
    }
}

struct Approved {}

impl State for Approved {
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        // reset to PendingReview. Might as well leave as is (return self)
        Box::new(PendingReview::new())
    }

    fn approve(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn content<'a>(&self, post: &'a Post) -> &'a str {
        &post.content
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    fn post_factory() -> Post {
        let mut post = Post::new();
        post.add_text("I ate a salad for lunch today");
        post
    }

    #[test]
    fn post_content_empty_if_draft() {
        let post = post_factory();

        assert_eq!("", post.content());
    }

    #[test]
    fn post_may_add_text_to_draft() {
        let mut post = post_factory();

        post.add_text(", really.");

        assert_eq!("I ate a salad for lunch today, really.", post.preview());
    }

    #[test]
    fn post_request_review() {
        let mut post = post_factory();

        post.request_review();
        assert_eq!("", post.content());
    }

    #[test]
    fn post_may_not_add_text_to_pending_review() {
        let mut post = post_factory();
        post.request_review();

        post.add_text(", really.");

        assert_eq!("I ate a salad for lunch today", post.preview());
    }

    #[test]
    fn post_one_approval_is_not_enough() {
        let mut post = post_factory();

        post.request_review();
        post.approve();

        assert_eq!("", post.content());
    }

    #[test]
    fn post_two_approvals_are_ok() {
        let mut post = post_factory();

        post.request_review();
        post.approve();
        post.approve();

        assert_eq!("I ate a salad for lunch today", post.content());
    }
}
