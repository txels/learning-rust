use blog::Post;

mod lib2;

fn main() {
    {
        // state pattern
        let mut post = Post::new();

        post.add_text("I ate a salad for lunch today");
        assert_eq!("", post.content());

        post.request_review();
        assert_eq!("", post.content());

        post.approve();
        post.approve();
        assert_eq!("I ate a salad for lunch today", post.content());
    }

    {
        // transitions as types
        let post = lib2::Post::new()
            .add_text("I ate a salad for lunch today")
            .request_review()
            .approve();
        assert_eq!("I ate a salad for lunch today", post.content());
        //if let lib2::Post{..} = post {
            //println!("Approved");
        //}
    }
}
