pub struct Countdown {
    value: u32,
}

impl Countdown {
    pub fn new(value: u32) -> Countdown {
        Countdown { value }
    }
}

impl Iterator for Countdown {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {
        if self.value == 0 {
            None
        } else {
            let v = self.value;
            self.value -= 1;
            Some(v)
        }
    }
}

// iterator over float ranges

pub struct FRange {
    val: f64,
    end: f64,
    incr: f64,
}

pub fn range(x1: f64, x2: f64, skip: f64) -> FRange {
    FRange {
        val: x1,
        end: x2,
        incr: skip,
    }
}

impl Iterator for FRange {
    type Item = f64;

    fn next(&mut self) -> Option<Self::Item> {
        let res = self.val;
        if res >= self.end {
            None
        } else {
            self.val += self.incr;
            Some(res)
        }
    }
}

#[cfg(test)]
mod test {
    use std::cmp::max;

    use super::*;

    #[test]
    fn iterator_next() {
        let v1 = vec![1, 2, 3];

        let mut v1_iter = v1.iter();

        assert_eq!(v1_iter.next(), Some(&1));
        assert_eq!(v1_iter.next(), Some(&2));
        assert_eq!(v1_iter.next(), Some(&3));
        assert_eq!(v1_iter.next(), None);
    }

    #[test]
    fn iterator_sum() {
        let v1 = vec![1, 2, 3];
        let v1_iter = v1.iter();
        let sum: i32 = v1_iter.sum();
        assert_eq!(sum, 6);
    }

    #[test]
    fn iterator_fold() {
        let v1 = [1, 2, 3];
        //let v1 = vec![1, 2, 3];
        let v1_iter = v1.iter();
        assert_eq!(v1_iter.fold(3, |acc, elem| acc + elem), 9);
    }

    #[test]
    fn iterator_reduce() {
        let v1 = [1, 2, 3, -2, 0];
        let v1_iter = v1.iter();
        let reduced = v1_iter.reduce(|acc, elem| max(acc, elem));
        assert_eq!(reduced, Some(&3));
    }

    #[test]
    fn iterator_map() {
        let v1 = vec![1, 2, 3];
        let v1_iter = v1.iter();
        let squares: Vec<_> = v1_iter.map(|elem| elem * elem).collect();
        assert_eq!(squares, vec![1, 4, 9]);
    }

    #[test]
    fn iterator_filter() {
        let v1 = [1, 2, 3];
        let v1_iter = v1.iter();
        let squares: Vec<&i32> = v1_iter.filter(|elem| **elem > 1).collect();
        assert_eq!(squares, vec![&2, &3]);
    }

    #[test]
    fn countdown() {
        let cd: Vec<u32> = Countdown::new(5).collect();
        assert_eq!(cd, [5, 4, 3, 2, 1]);
    }

    #[test]
    fn float_ranges() {
        let floats: Vec<f64> = range(0.0, 0.3, 0.1).collect();
        assert_eq!(floats, vec![0.0, 0.1, 0.2]);
    }

    #[test]
    fn iterator_kinds() {
        let v1 = vec!["One".to_string(), "Two".to_string(), "Three".to_string()];
        let mut v2 = vec!["One".to_string(), "Two".to_string(), "Three".to_string()];
        let v3 = vec!["One".to_string(), "Two".to_string(), "Three".to_string()];

        for i1 in v1.iter() { // i1 in &v1
            // can't do because iter borrows immutably
            //*i1 = "overwrite".to_string();
            println!("{}", i1);
        }
        for i2 in v2.iter_mut() { // i2 in &mut v2
            *i2 = "overwrite".to_string();
            println!("{}", i2);
        }
        for i3 in v3.into_iter() { // i3 in &mut v3
            println!("{}", i3);
        }

        assert_eq!(v1[0], "One");
        assert_eq!(v2[0], "overwrite");
        // can't do as `into_iter` moves values
        //assert_eq!(v3[0], "One");
    }
}
