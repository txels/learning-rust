#[derive(Debug)]
struct Rectangle<'a> {
    width: u32,
    height: u32,
    name: &'a str,
}

impl Rectangle<'_> {
    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width >= other.width && self.height >= other.height
    }
}


impl PartialEq for Rectangle<'_> {
    fn eq(&self, other: &Rectangle) -> bool {
        self.width == other.width && self.height == other.height
    }
}


pub fn add(x: i32, y: i32) -> i32 {
    x + y
}

// annotation to only compile and include module when testing
#[cfg(test)]
mod tests {
    use super::Rectangle;

    #[test]
    fn adding() {
        assert_eq!(2 + 2, 4);
    }
    #[test]
    fn adding_ko() {
        assert_ne!(2 + 2, 5);
    }
    #[test]
    fn bools() {
        assert!(true);
    }
    #[test]
    #[should_panic(expected = "fast")]
    fn boolpanic() {
        panic!("Fail fast");
    }

    #[test]
    fn holds_ok() {
        let r1 = Rectangle{width: 1, height: 1, name: "Pete"};
        let r1b = Rectangle{width: 1, height: 1, name: "Petra"};
        let r2 = Rectangle{width: 2, height: 1, name: "Joe"};
        let r3;
        {
            let s = "joe";
            r3 = Rectangle{width: 2, height: 1, name: s};
        }
        assert!(r2.can_hold(&r1));
        assert!(!r1.can_hold(&r2));
        assert_ne!(r1.width, r2.width);
        assert_ne!(r1, r2);
        assert_eq!(r1, r1b);
        assert_eq!(r2, r3);
    }

    #[test]
    fn with_result() -> Result<(), String> {
        if 2 + 2 == 4 {
            Ok(())
        } else {
            Err(String::from("two plus two does not equal four"))
        }
    }
}
