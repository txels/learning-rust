use testing; // name of the crate

mod utils;

#[test]
fn it_adds() {
    utils::setup();
    assert_eq!(4, testing::add(2, 2));
}

#[test]
fn it_adds_signed() {
    assert_eq!(0, testing::add(2, -2));
}
