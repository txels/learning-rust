fn sqr(x: f64) -> f64 {
    x * x
}

fn main() {
    let mut data = 23;
    let mut sum = 0.0;
    for i in 1..43 {
        // let s = i.to_string();
        // let value = if i == 22 { String::from("Catch 22!!") } else {i.to_string()};
        let value = if i == 22 { "Catch 22!!" } else { "" };
        data = i;
        sum += i as f64;
        print!("{}", value);
    }
    assert_eq!(data, 42);
    println!("\nSUM: {}", sum);
    println!("sqr: {}", sqr(sum));
    println!("My name is {0}, {1} {0}", "Bond", "James");
    println!("My name is {name}, {surname} {name}", name="Mary", surname="Contrary");
    println!("My name is {0}, {surname} {0}", "Joe", surname="Example");
    println!("No padding {number:>width$}", number=1, width=6);
    println!("0 padding  {number:0>width$}", number=1, width=6);
    println!("Pi is approx  {pi:1.2}", pi=3.14159);
}
