fn main() {
    let x = 13;
    let j = {
        let y = x * 4;
        (8, y + x)
    };
    println!("{}", j.1);

    let res = if j.1 > 12 {
        "greater than 12!"
    } else if j.1 < 2 {
        "mini!"
    } else {
        "boo!"
    };
    println!("{}", res);

    let mut count = 22;
    let multof7 = loop {
        count += 2;
        if count % 7 == 0 {
            break count;
        }
    };
    println!("{} is a multiple of 7", multof7);
}
