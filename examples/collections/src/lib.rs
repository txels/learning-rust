use std::collections::HashMap;
use std::collections::HashSet;
use std::sync::Arc;

const Alphafilter: &Fn(char) -> bool = &|c: char| !c.is_alphabetic();
// within a function scope it can be defined as:
// let Alphafilter: Box<Fn (char) -> bool> = Box::new(|c: char| !c.is_alphabetic());

pub fn counter(text: &str) -> HashMap<String, u32> {
    let mut map = HashMap::new();

    for s in text.split(Alphafilter) {
        let word = s.to_lowercase();
        if word.len() > 0 {
            let mut count = map.entry(word).or_insert(0);
            *count += 1;
        }
    }

    map
}

fn make_set(words: &str) -> HashSet<&str> {
    // implicitly collect into a hashset because of fn return type
    words.split(Alphafilter).collect()
}

use std::hash::Hash;

trait ToSet<T> {
    fn to_set(self) -> HashSet<T>;
}

impl<T, I> ToSet<T> for I
where
    T: Eq + Hash,
    I: Iterator<Item = T>,
{
    fn to_set(self) -> HashSet<T> {
        self.collect()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn vec_stringlike_methods() {
        let v = vec![1, 2, 3, 4];
        let v2: Vec<i32> = (1..).take(5).collect();

        let (p1, p2) = v2.split_at(3);

        assert_eq!(p1, vec![1, 2, 3]);
        assert_eq!(p2, vec![4, 5]);
        assert!(p1.starts_with(&[1, 2]));
        assert!(p2.contains(&4));
        assert!(v.contains(&4));
    }

    #[test]
    fn vec_iter_methods() {
        let v = vec![10, 20, 30, 40, 50];
        assert_eq!(v.iter().position(|&i| i == 30).unwrap(), 2);
    }

    #[test]
    fn vec_extend() {
        let mut vec = vec![1, 2, 3];
        vec.extend([7, 8, 9].iter());

        assert_eq!(vec.len(), 6);
    }

    #[test]
    fn vec_iter_consume() {
        let v = vec![1];

        for i in v {
            // value moved, used `into_iter`
            assert_eq!(i, 1);
        }

        // not possible as v has been moved & consumed:
        // assert_eq!(v.len(), 1);
    }

    #[test]
    fn vec_iter_borrow() {
        let v = vec![1];

        for i in &v {
            // value borrowed, i is reference
            assert_eq!(i, &1);
        }

        assert_eq!(v.len(), 1);
    }

    #[test]
    fn collect_errors() {
        let nums = ["5", "52", "65"];
        let iter = nums.iter().map(|s| s.parse::<i32>());
        // rust collects "unwrapped" results
        let converted: Result<Vec<_>, _> = iter.collect();
        assert_eq!(Ok(vec![5, 52, 65]), converted);
        let nums = ["5", "bad", "65"];
        let iter = nums.iter().map(|s| s.parse::<i32>());
        // rust collects "unwrapped" results
        let converted: Result<Vec<_>, _> = iter.collect();
        assert!(converted.is_err());
    }

    #[test]
    fn test_hashmap() {
        let text = "Now is the winter of our discontent made glorious summer, by this sun of York. And the gods that looked upon our house may rejoice with our happinness.";
        let counted = counter(text);
        assert_eq!(counted["the"], 2);
        assert_eq!(counted["our"], 3);
        let mut entries: Vec<_> = counted.into_iter().collect();
        entries.sort_by(|a, b| b.1.cmp(&a.1));
        let mut top5: Vec<_> = entries.iter().take(3).map(|x| &x.0).collect();
        top5.sort();
        assert_eq!(top5, vec!["of", "our", "the"]);
    }

    #[test]
    fn test_make_set() {
        let text = "Now is the winter of our discontent made glorious summer, by this sun of York. And the gods that looked upon our house may rejoice with our happinness.";
        let seasons = "winter summer autumn spring";
        let set = make_set(text);
        let seasons = make_set(seasons);
        assert_eq!(set.len(), 25);
        let mut inters: Vec<&&str> = set.intersection(&seasons).collect();
        inters.sort();
        assert_eq!(inters, vec![&"summer", &"winter"]);
    }

    #[test]
    fn test_to_set_trait() {
        let text = make_set("Now is the winter of our discontent made glorious summer, by this sun of York. And the gods that looked upon our house may rejoice with our happinness.");
        let seasons = make_set("winter summer autumn spring");
        let inters = text.intersection(&seasons).cloned().to_set();
        assert_eq!(inters.len(), 2);
    }
}
