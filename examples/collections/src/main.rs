// playing with various types of containers
use std::collections::HashMap;

#[derive(Debug)]
enum SpreadsheetCell {
    Int(i32),
    Float(f64),
    Text(String),
}

fn main() {
    //vectors();
    //strings();
    hashmaps();
}

fn vectors() {
    let mut v: Vec<u8> = vec![1, 2, 3];

    v.push(12);

    let third_copy = v[2];
    let third = &v[2];
    println!("The third element (borrowed) is {}", third);

    v.push(22);

    match v.get(2) {
        Some(third) => println!("The third element (fet) is {}", third),
        None => println!("There is no third element."),
    }

    println!("{:?}", v);
    println!("The third element (copied) is {}", third_copy);

    // mutate upon iteration: need a `mut` reference
    for i in &mut v {
        *i += 2;
        println!("{}", i);
    }
    println!("{:?}", v);

    // variable elements using enums...
    let row = vec![
        SpreadsheetCell::Int(3),
        SpreadsheetCell::Text(String::from("blue")),
        SpreadsheetCell::Float(10.12),
    ];
    println!("{:?}", row);

    for c in row {
        match c {
            SpreadsheetCell::Text(s) => println!("{}", s),
            _ => println!("why bother..."),
        }
    }

    for d in v.drain(1..3) {
        println!("{:?}", d);
    }
    let x: Vec<_> = v.drain(2..).collect();
    assert_eq!(x, &[24]);
    println!("Drained = {:?}", v);
}

fn strings() {
    let _s = String::new();
    let hell = "hell darkness my old friend".to_string();
    println!("{:#?}", hell);
    let korean = String::from("안녕하세요");
    println!("{}", korean);
    assert_eq!("안녕하세요".to_string(), String::from("안녕하세요"));

    let s1 = String::from("Hello, ");
    let s2 = String::from("world!");
    let s3 = s1 + &s2;
    let s3 = format!("{} {}", s3, s2);
    println!("{}", s3);

    for ch in korean.chars() {
        println!("{}", ch);
    }
}

fn hashmaps() {
    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 20);
    println!("{:?}", scores);

    scores.entry(String::from("Red")).or_insert(50);
    scores.entry(String::from("Blue")).or_insert(50);

    println!("{:?}", scores);

    {
        let text = "hello world wonderful world";
        let mut map = HashMap::new();

        for word in text.split_whitespace() {
            let count = map.entry(word).or_insert(0);
            *count += 1;
        }

        println!("{:?}", map);
    }
}
