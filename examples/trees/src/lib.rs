use std::fmt::Display;

type NodeBox<T> = Option<Box<Node<T>>>;

#[derive(Debug)]
pub struct Node<T> {
    payload: T,
    left: NodeBox<T>,
    right: NodeBox<T>,
}

impl<T> Node<T>
where
    T: PartialOrd + Display,
{
    pub fn new(s: T) -> Node<T> {
        Node {
            payload: s,
            left: None,
            right: None,
        }
    }

    pub fn insert(&mut self, data: T) {
        if data < self.payload {
            match self.left {
                Some(ref mut n) => n.insert(data),
                None => self.set_left(Self::new(data)),
            }
        } else {
            match self.right {
                Some(ref mut n) => n.insert(data),
                None => self.set_right(Self::new(data)),
            }
        }
    }

    pub fn visit(&self) -> String {
        let mut lhs: String = String::new();

        if let Some(ref left) = self.left {
            lhs += &left.visit();
            if lhs != "" {
                lhs += ":";
            }
        }
        lhs += &format!("{}", self.payload);
        if let Some(ref right) = self.right {
            let rhs = &right.visit();
            if rhs != "" {
                lhs += ":"
            }
            lhs += rhs;
        }
        lhs
    }

    fn boxer(node: Node<T>) -> NodeBox<T> {
        Some(Box::new(node))
    }

    fn set_left(&mut self, node: Node<T>) {
        self.left = Self::boxer(node);
    }

    fn set_right(&mut self, node: Node<T>) {
        self.right = Self::boxer(node);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn visit_works() {
        let mut root = Node::new("root".to_string());
        root.insert("one".to_string());
        root.insert("three".to_string());
        root.insert("gazillion".to_string());
        root.insert("two".to_string());
        root.insert("four".to_string());

        let visited = root.visit();
        assert_eq!(&visited, "four:gazillion:one:root:three:two");
    }
}
