#[derive(Debug, PartialEq)]
enum Color {
    RGB(u32, u32, u32),
    RGBA(u32, u32, u32, u32),
    //HSV(u32, u32, u32),
}

enum Message {
    Quit,
    //Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(u32, u32, u32),
}

impl Message {
    fn print(&self) -> String {
        match &self {
            Message::Quit => String::from("QUIT"),
            Message::Write(s) => format!("{}", s),
            Message::ChangeColor(r,g,b) => format!("{:?}", Color::RGB(*r, *g, *b)),
        }
    }
}

fn main() {
    let red = Color::RGB(255, 0, 0);
    let red2 = Color::RGB(255, 0, 0);
    assert_eq!(red, red2);
    let greenish = Color::RGBA(0, 255, 0, 255);
    println!("{:?}", red);
    println!("{:?}", greenish);

    let msg = Message::Write("hello".to_string());
    println!("{}", msg.print());
    let msg = Message::Quit;
    println!("{}", msg.print());
    let msg = Message::ChangeColor(12, 12, 34);
    println!("{}", msg.print());

    if let Message::ChangeColor(r, 12, 34) = msg {
        println!("Matched with r = {}", r);
    }
}
