#[derive(Debug,PartialEq,Copy,Clone)]
enum Direction {
    // four variants:
    Up,
    Right,
    Down,
    Left,
}

/// Iterate by endlessly looping through variants
impl Iterator for Direction {
    type Item = Self;
    fn next(&mut self) -> Option<Self> {
        use Direction::*;
        *self = match *self {
            Up => Right,
            Right => Down,
            Down => Left,
            Left => Up,
        };
        // Return a mutated version with its new direction
        Some(*self)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn iterate_next() {
        let mut init_dir: Direction = Direction::Up;
        let mut next: Option<Direction> = init_dir.next();
        assert_eq!(Some(Direction::Right), next);
        next = next.unwrap().next();
        assert_eq!(Some(Direction::Down), next);
        next = next.unwrap().next();
        assert_eq!(Some(Direction::Left), next);
    }

    #[test]
    fn take_and_collect() {
        use Direction::*;
        let init_dir: Direction = Direction::Up;
        let collect: Vec<Direction> = init_dir.take(6).collect();
        assert_eq!(collect, vec![Right, Down, Left, Up, Right, Down]);
    }

    #[test]
    fn iterate_loop() {
        let init_dir = Direction::Up;
        let end_dir: Option<Direction>;
        {
            let mut dir: Option<Direction> = Some(init_dir);
            loop {
                dir = dir.unwrap().next();
                if dir.unwrap() == init_dir {
                    end_dir = dir;
                    break;
                }
            }
        }
        assert_eq!(end_dir, Some(init_dir));
    }

    #[test]
    fn iterate_for() {
        let init_dir = Direction::Up;
        let mut end_dir: Direction = Direction::Down;
        let mut count = 0;
        for (i, dir) in init_dir.enumerate() {
            if dir == init_dir {
                end_dir = dir;
                count = i + 1;
                break;
            }
        }
        assert_eq!(end_dir, init_dir);
        assert_eq!(count, 4);
    }
}
