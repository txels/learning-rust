pub fn prepend(x: &str) -> String {
    let mut s = String::from("PFX> ");
    s.push_str(x);
    s
}

pub fn default() -> String {
    let s = String::from("default value");
    s
}

pub fn add_one(x: i32) -> i32 {
    x + 1
}

pub fn do_twice(f: fn(i32) -> i32, arg: i32) -> i32 {
    f(arg) + f(arg)
}

pub fn measure(f: fn(&str) -> String, arg: &str) -> u32 {
    f(arg).len() as u32
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn fn_pointer() {
        let answer = do_twice(add_one, 5);

        assert_eq!(answer, 12);
    }

    #[test]
    fn fn_pointer2() {
        let answer = measure(prepend, "this");
        assert_eq!(answer, 9);
    }

    #[test]
    fn fn_pointer_as_closure() {
        let x = Some(String::from("Mystery"));
        assert_eq!(x.unwrap_or_else(default), "Mystery".to_string());
    }

    #[test]
    fn fn_pointer_as_closure_none() {
        let x: Option<String> = None;
        assert_eq!(x.unwrap_or_else(default), "default value".to_string());
    }

    #[test]
    fn fn_to_string_as_closure() {
        let list_of_numbers = vec![1, 2, 3];
        let list_of_strings: Vec<String> =
            list_of_numbers.iter().map(ToString::to_string).collect();
        assert_eq!(
            list_of_strings,
            vec!["1".to_string(), "2".to_string(), String::from("3")]
        );
    }
    #[test]
    fn fn_tuple_constructor_as_closure() {
        #[derive(PartialEq, Debug)]
        enum Status {
            Value(u32),
            Stop,
        }

        let list_of_statuses: Vec<Status> = (0u32..20).map(Status::Value).collect();

        assert_eq!(list_of_statuses[0], Status::Value(0));
    }
}
