use std::fmt::Display;


struct Pair<T> {
    x: T,
    y: T,
}

impl<T> Pair<T> {
    fn new(x: T, y: T) -> Self {
        Self { x, y }
    }
}

// applies on all pairs of T where T has both Display and PartialOrd traits
impl<T: Display + PartialOrd> Pair<T> {
    fn cmp_display(&self) {
        if self.x >= self.y {
            println!("The largest member is x = {}", self.x);
        } else {
            println!("The largest member is y = {}", self.y);
        }
    }
}

pub trait GreaterThan<T:PartialOrd> {
    fn greater_than(&self, value: &T) -> bool;
}

// blanket trait "GreaterThan" applied to all types with PartialOrd trait
impl<T:PartialOrd> GreaterThan<T> for T {
    fn greater_than(&self, value: &T) -> bool {
        self > value 
    }
}


pub trait Summary {
    // declaration with no implementation
    fn summarize(&self) -> String;
    // declaration with default implementation (optional)
    //fn summarize(&self) -> String {
        //String::from("(Read more...)")
    //}
}

pub struct NewsArticle {
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
}

impl Summary for NewsArticle {
    fn summarize(&self) -> String {
        format!("{}, by {} ({})", self.headline, self.author, self.location)
    }
}

pub struct Tweet {
    pub username: String,
    pub content: String,
    pub reply: bool,
}

impl Summary for Tweet {
    fn summarize(&self) -> String {
        format!("{}: {}", self.username, self.content)
    }
}

pub fn print_summary(item: &impl Summary) {
    println!("{}", item.summarize());
}


struct SelectBox {
    width: i32,
    height: i32,
    options: Vec<String>,
}

use traits::{Draw, Button, Screen};

impl Draw for SelectBox {
    fn draw(&self) {
        // ...
    }
}

fn trait_objects() {
    let screen = Screen {
        components: vec![
            Box::new(Button{ width: 12, height: 22, label: "Accept".to_owned()}),
            Box::new(SelectBox{ width: 12, height: 22, options: vec!["one".to_owned()]}),
        ]
    };

    screen.run();
}

fn main() {
    let tweet = Tweet {
        username: String::from("txels"),
        content: String::from("Rust rules"),
        reply: false,
    };
    print_summary(&tweet);

    let p = Pair::new(21, 12);
    p.cmp_display();
    Pair::new("dolly", "hello").cmp_display();
    Pair::new(true, false).cmp_display();

    // new blanket trait
    assert!(13.greater_than(&4));
    assert!("hello".greater_than(&"dolly"));
    assert!("hello".to_string().greater_than(&"dolly".to_string()));

    trait_objects();
}
