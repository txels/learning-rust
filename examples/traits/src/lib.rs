pub trait Draw {
    fn draw(&self);
}

pub struct Screen {
    pub components: Vec<Box<dyn Draw>>,
}

impl Screen {
    pub fn run(&self) {
        for component in self.components.iter() {
            component.draw();
        }
    }
}

// make Hash traits available...
use std::hash::{Hash, Hasher};

#[derive(Debug,Default,PartialEq,Eq,PartialOrd,Ord,Clone,Hash)]
pub struct Button {
    pub width: u32,
    pub height: u32,
    pub label: String,
}

impl Draw for Button {
    fn draw(&self) {
        // code to actually draw a button
    }
}

#[derive(Debug, PartialEq, Copy, Clone)]
struct Rational {
    n: u32,
    d: u32,
}

impl std::ops::Mul for Rational {
    type Output = Self;
    fn mul(self, rhs: Self) -> Self {
        Rational{ n : self.n * rhs.n, d : self.d * rhs.d }
    }
}

pub fn sqr<T> (x: T) -> T::Output
where T: std::ops::Mul + Copy {
    x * x
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn squareable() {
        let x = Rational{n: 1, d: 3};
        assert_eq!(sqr(x), Rational{n: 1, d: 9});
    }

    #[test]
    fn auto_traits_default() {
        let btn = Button::default();
        assert_eq!(btn.width, 0);
        assert_eq!(btn.height, 0);
        assert_eq!(btn.label, "");
        let btn = Button{ width:20, ..Button::default()};
        assert_eq!(btn.width, 20);
        assert_eq!(btn.height, 0);
        assert_eq!(btn.label, "");
    }

    #[test]
    fn auto_traits_hash_eq() {
        let btn = Button{ width:20, ..Button::default()};
        let mut hasher = std::collections::hash_map::DefaultHasher::new();
        btn.hash(&mut hasher);
        let res = hasher.finish();
        assert_eq!(res, 17155559041820822382);

        let mut map = std::collections::HashMap::new();
        map.insert(&btn, 122);
        assert_eq!(map[&btn], 122);
    }

    #[test]
    fn auto_traits_ordering() {
        let btn = Button{ width:20, ..Button::default()};
        let btn2 = Button{ width:10, ..Button::default()};
        assert!(btn > btn2);
        assert_eq!(btn.cmp(&btn2), std::cmp::Ordering::Greater);
    }

    #[test]
    fn auto_traits_clone() {
        let btn = Button{ width:20, ..Button::default()};
        let btn2 = btn.clone();
        assert!(btn == btn2);
        assert_eq!(btn.cmp(&btn2), std::cmp::Ordering::Equal);
    }
}
