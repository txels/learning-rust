// examples of patterns
fn main() {
    {
        // while let
        let mut stack = Vec::new();

        stack.push(1);
        stack.push(2);
        stack.push(3);

        // loop while the pattern matches
        while let Some(top) = stack.pop() {
            println!("{}", top);
        }
    }

    {
        // "destructure" tuples
        for (i, val) in (5..=9).enumerate() {
            println!("{}: {}", i, val);
        }
    }

    {
        #[derive(Debug)]
        enum Tomato {
            Ripe,
            Green,
        }
        // "destructure" tuples as references (don't take ownership)
        let tomatoes = (Tomato::Green, Tomato::Ripe);
        let (ref j, ref k) = tomatoes;
        println!("\n---\nTOMATOES!");
        println!("j={:?}", j);
        println!("k={:?}", k);
        println!("tomato.0={:?}\n\n", tomatoes.0);
    }

    {
        // let pattern = expression
        //
        let (j, _) = (24, "Hello");
        println!("j={}", j);
    }

    {
        // function params
        fn printuple(&(i, val): &(i32, i32)) {
            println!("{}: {}", i, val);
        }

        printuple(&(12, 34));
    }

    {
        // match options

        let x: u8 = 12;

        match x {
            0 => println!("zero"),
            1..=5 => println!("smaller than 5"),
            6 | 7 => println!("six or seven"),
            8..=25 => println!("smaller than 25"),
            x if x <= 254 => println!("smaller than 254"),
            _ => println!("other (255)"),
        }
    }

    {
        //destructure structs

        struct Point {
            x: i32,
            y: i32,
        }
        let p = Point { x: 0, y: 7 };

        // destructure point into a, b
        let Point { x: a, y: b } = p;
        assert_eq!(0, a);
        assert_eq!(7, b);
        let Point { x: c, .. } = p;
        assert_eq!(0, c);

        // destructure and match
        match p {
            Point { x, y: 0 } => println!("On the x axis at {}", x),
            Point { x: 0, y } => println!("On the y axis at {}", y),
            Point { x, y } => println!("On neither axis: ({}, {})", x, y),
        }
    }

    {
        // destructure+match structs and enums (nested)
        enum Color {
            Rgb(i32, i32, i32),
            Hsv(i32, i32, i32),
        }

        enum Message {
            Quit,
            Move { x: i32, y: i32 },
            Write(String),
            ChangeColor(Color),
        }

        let msg = Message::ChangeColor(Color::Hsv(0, 160, 255));

        match msg {
            Message::ChangeColor(Color::Rgb(r, g, b)) => {
                println!("Change the color to red {}, green {}, and blue {}", r, g, b)
            }
            Message::ChangeColor(Color::Hsv(h, s, v)) => println!(
                "Change the color to hue {}, saturation {}, and value {}",
                h, s, v
            ),
            _ => (),
        }
    }

    {
        // ellipsis, other uses
        let numbers = (2, 4, 8, 16, 32);

        match numbers {
            (first, .., last) => {
                println!("first and last: {}, {}", first, last);
            }
        }
    }

    {
        // @-bind for conditions in struct matches
        enum Message {
            Hello { id: i32 },
        }

        let msg = Message::Hello { id: 5 };

        match msg {
            Message::Hello {
                // will match if id in range, and bind id_variable
                id: id_variable @ 3..=7,
            } => println!("Found an id in range: {}", id_variable),
            Message::Hello { id: 10..=12 } => {
                println!("Found an id in another range")
            }
            Message::Hello { id } => println!("Found some other id: {}", id),
        }
    }

    {
        // match parse, use type hint
        if let Ok(n) = "42".parse::<u32>() {
            println!("Parsed u32 = {}", n);
        }
    }
}
