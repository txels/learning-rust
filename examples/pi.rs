use std::f64::consts;

fn main() {
    let pi = consts::PI;
    println!("{}", pi);
    println!("{}", (2.0 * pi).cos());
}
