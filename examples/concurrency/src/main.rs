use std::sync::mpsc;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

fn threads() {
    let handle = thread::spawn(|| {
        for i in 1..10 {
            println!("hi number {} from the spawned thread!", i);
            thread::sleep(Duration::from_millis(2));
        }
    });

    {
        for i in 1..5 {
            println!("hi number {} from the main thread!", i);
            thread::sleep(Duration::from_millis(1));
        }
        println!("Main thread done.");
    }

    handle.join().unwrap();
}

fn thread_move() {
    let v = vec![1, 2, 3];

    let handle = thread::spawn(move || {
        println!("Here's a vector: {:?}", v);
    });

    handle.join().unwrap();
}

fn thread_channels() {
    let (tx1, rx) = mpsc::channel();
    let tx2 = tx1.clone();

    let hrecv = thread::spawn(move || loop {
        match rx.recv() {
            Ok(msg) => println!("recv={}", msg),
            _ => break,
        }
    });
    thread::spawn(move || {
        for x in 1..10 {
            let val = String::from(format!("one {}", x));
            tx1.send(val).unwrap();
            thread::sleep(Duration::from_millis(20));
            // println!("{}", val); value moved, can't use
        }
    });
    thread::spawn(move || {
        for x in 1..10 {
            let val = String::from(format!("two {}", x));
            tx2.send(val).unwrap();
            thread::sleep(Duration::from_millis(80));
            // println!("{}", val); value moved, can't use
        }
    });

    // hsend.join().unwrap();
    hrecv.join().unwrap();
}

fn thread_mutex() {
    let counter = Arc::new(Mutex::new(0));
    let mut handles = vec![];

    for _i in 0..3 {
        let c = Arc::clone(&counter);
        let t = thread::spawn(move || {
            // returns a smart pointer `MutexGuard` that implements Drop to release the lock
            let mut num = c.lock().unwrap();
            *num += 1;
        });
        handles.push(t);
    }

    for handle in handles {
        handle.join().unwrap();
    }
    println!("counter = {:?}", *counter.lock().unwrap());
}

fn main() {
    println!("\nTHREADS\n--------");
    threads();
    println!("\nTHREADS (move)\n--------");
    thread_move();
    println!("\nTHREADS (channels)\n--------");
    thread_channels();
    println!("\nTHREADS (mutex)\n--------");
    thread_mutex();
}
