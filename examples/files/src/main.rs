use std::error::Error;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::os::linux::fs::MetadataExt;
use std::os::unix::fs::PermissionsExt;
use std::path::Path;

fn read_all_lines(filename: &Path) -> io::Result<Vec<String>> {
    let file = File::open(filename)?;
    let mut lines = Vec::new();
    let reader = io::BufReader::new(file);

    for line in reader.lines() {
        lines.push(line?);
    }
    Ok(lines)
}

fn read_line_by_line(filename: &Path) -> io::Result<Vec<String>> {
    let file = File::open(filename)?;
    let mut lines = Vec::new();
    let mut reader = io::BufReader::new(file);
    let mut buf = String::with_capacity(200);

    while reader.read_line(&mut buf)? > 0 {
        lines.push(buf.trim_end().to_string());
        buf.clear();
    }
    Ok(lines)
}

fn main() -> Result<(), Box<dyn Error>> {
    let path = Path::new("../../minigrep/nobody.txt");

    println!("{}:{}:{}", file!(), line!(), column!());
    println!("OS: {:?}", std::env::consts::OS);
    println!("Canonical path: {:?}", path.canonicalize()?);
    println!("Extension: {:?}", path.extension());
    println!("Metadata:");
    match path.metadata() {
        Ok(data) => {
            println!(" - type {:?}", data.file_type());
            println!(" - len {}", data.len());
            println!(" - perm {:o}", data.permissions().mode());
            println!(" - created {:?}", data.created());
            println!(" - modified {:?}", data.modified());
            if std::env::consts::OS == "linux" {
                println!(" - mode {:?}", data.st_mode());
                println!(" - uid {:?}", data.st_uid());
                println!(" - gid {:?}", data.st_gid());
                println!(" - blksize {:?}", data.st_blksize());
                println!(" - blocks {:?}", data.st_blocks());
            }
        }
        Err(e) => println!("error {:?}", e),
    }

    // Can be done with pathbuf, then use &pathbuf === path
    //let pathbuf = PathBuf::from("../../minigrep/nobody.txt");
    //

    if let Ok(x) = read_all_lines(path) {
        println!("All lines\n{:#?}\n---\n", x);
    };

    if let Ok(_x) = read_line_by_line(path) {
        //println!("Line by line\n{:#?}\n---\n", x);
    };

    Ok(())
}
