// structs

struct User {
    username: String,
    email: String,
    age: Option<i8>,
}

struct Color(i32, i32, i32);

fn main() {
    let _green = Color(0, 255, 0);
    let username = String::from("Potato");

    let mut email = String::from(&username);
    email.push_str("@example.com");
    let user = User {
        username, // shortand syntax similar to ES6
        age: None,
        email,
    };

    let mut email = String::from(&user.username);
    email.push_str("@example.com");
    let _user2 = User {
        username: String::from(&user.username),
        email,
        age: Some(12),
    };

    println!("name={}", user.username);
    println!("email={}", user.email);
    println!(
        "age={}",
        match user.age {
            Some(x) => x.to_string(),
            None => "?".to_string(),
        }
    );
}
