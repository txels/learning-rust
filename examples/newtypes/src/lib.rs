use std::collections::HashMap;

pub struct Users(HashMap<u32, String>);

impl Users {
    pub fn new() -> Users {
        Users(HashMap::new())
    }

    pub fn insert(&mut self, name: String) -> u32 {
        let nkeys: u32 = self.0.len() as u32;
        self.0.insert(nkeys, name);
        nkeys
    }

    pub fn get<'a>(&'a self, index: u32) -> &'a String {
        self.0.get(&index).unwrap()
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn add_user() {
        let mut users = Users::new();

        let u1 = "hello";
        let pos = users.insert(u1.to_string());
        let user = users.get(pos);
        assert_eq!(user, u1);

        let u2 = "byebut";
        let pos = users.insert(u2.to_string());
        let user = users.get(pos);
        assert_eq!(user, u2);

        assert_eq!(users.0.len(), 2);
    }
}
