use std::ops::Add;

#[derive(Debug, PartialEq)]
struct Point {
    x: i32,
    y: i32,
}

struct Millimeters(u32);
struct Meters(u32);

// overload the `+` operator
impl Add<Meters> for Millimeters {
    type Output = Millimeters;

    fn add(self, other: Meters) -> Millimeters {
        Millimeters(self.0 + (other.0 * 1000))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    //#[test]
    //fn add_points() {
    //impl Add for Point {
    //type Output = Point;

    //fn add(self, other: Point) -> Point {
    //Point {
    //x: self.x + other.x,
    //y: self.y + other.y,
    //}
    //}
    //}
    //assert_eq!(
    //Point { x: 1, y: 0 } + Point { x: 2, y: 3 },
    //Point { x: 3, y: 3 }
    //);
    //}

    #[test]
    fn add_points_as_i32() {
        impl Add for Point {
            type Output = i32;

            fn add(self, other: Point) -> i32 {
                self.x + other.x + self.y + other.y
            }
        }
        assert_eq!(Point { x: 1, y: 0 } + Point { x: 2, y: 3 }, 6);
    }

    #[test]
    fn add_meters() {
        let Millimeters(x) = Millimeters(240) + Meters(24);
        assert_eq!(x, 24240);
    }
}
