fn sum(values: &[i64]) -> i64 {
    let mut res = 0;
    for i in 0..values.len() {
        res += values[i]
    }
    res
}

fn main() {
    let arr: [i64; 3] = [10, 20, 30];
    let part = &arr[1..];
    for i in 0..arr.len() {
        println!("{} - {}", i, arr[i]);
    }
    for elem in &arr {
        println!("elem: {}", elem);
    }
    println!("sum: {}", sum(&arr));
    println!("arr: {:?}", arr);
    println!("part: {:?}", part);
    println!("&arr: {:?}", &arr);
    println!("slice.first: {:?}", &arr.get(0));

    let first = *part.get(0).unwrap();
    let second = *part.get(1).unwrap();
    println!("first+second: {}", first + second);
    println!("slice.first unwrapped: {}", first);
    println!("slice.outofbounds: {:?}", arr.get(3).unwrap_or(&-1));
}
