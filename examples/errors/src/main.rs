use std::error::Error;
use std::fs::{self, File};
use std::io::{self, prelude::*, ErrorKind};

mod guess;
use guess::Guess;

fn main() {
    //let f = open_match();
    let f = open_unwrap_basic();
    println!("{:?}", f);

    let username = match read_username_from_file_autopropagate() {
        Ok(username) => username,
        Err(e) => panic!("{:?}", e),
    };
    println!("{}", username);

    let g = Guess::new(-12);
    println!("{}", g.value());
}

pub fn open_match() -> File {
    let f = match File::open("hello.txt") {
        Ok(file) => file,
        Err(error) => match error.kind() {
            ErrorKind::NotFound => match File::create("hello.txt") {
                Ok(fc) => fc,
                Err(e) => panic!("Problem creating the file: {:?}", e),
            },
            other => {
                panic!("Problem opening the file: {:?}", other)
            }
        },
    };
    f
}

pub fn open_unwrap() -> File {
    let f = File::open("hello.txt").unwrap_or_else(|error| {
        if error.kind() == ErrorKind::NotFound {
            File::create("hello.txt").unwrap_or_else(|error| {
                panic!("Problem creating the file: {:?}", error);
            })
        } else {
            panic!("Problem opening the file: {:?}", error);
        }
    });
    f
}

pub fn open_unwrap_basic() -> File {
    //let f = File::open("hello.txt").unwrap();
    let f = File::open("hello.txt").expect("Opening hello.txt");
    f
}

pub fn read_username_from_file() -> Result<String, io::Error> {
    // match and propagate error manually
    let mut f = match File::open("hello.txt") {
        Ok(file) => file,
        Err(e) => return Err(e),
    };

    let mut s = String::new();

    // match and propagate error manually
    match f.read_to_string(&mut s) {
        Ok(_) => Ok(s),
        Err(e) => Err(e),
    }
}

/// Error propagation via ? plus explicit result
pub fn read_username_from_file_autopropagate() -> Result<String, io::Error> {
    let mut s = String::new();
    // ? will continue on Ok or propagate the error
    File::open("hello.txt")?.read_to_string(&mut s)?;
    // explicit Ok result
    Ok(s)
}

/// Propagate the io result/error from read_to_string
pub fn read_username_from_file_stdlib() -> Result<String, io::Error> {
    fs::read_to_string("hello.txt")
}

/// this will panic by acessing outside a vector
pub fn panicked() {
    //panic!("Hello, world!");
    let v = vec![1, 2, 3];
    v[99];
}
