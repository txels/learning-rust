// App-specific error:
// - May implement Debug
// - Must implement Display
// - Must implement Error

use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub struct MyError {
    details: String
}

// Custom result for this crate.
// This approach is common e.g. with io::Result<T> etc.
type Result<T> = std::result::Result<T, MyError>;

impl MyError {
    fn new(msg: &str) -> MyError {
        MyError { details: msg.to_string() }
    }
}

impl fmt::Display for MyError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl Error for MyError {
    fn description(&self) -> &str {
        &self.details
    }
}

// Make it possible to convert from ParseFloatError to MyError
use std::num::ParseFloatError;

impl From<ParseFloatError> for MyError {
    fn from(err: ParseFloatError) -> Self {
        MyError::new(err.description())
    }
}


pub fn gen_error(yes: bool) -> Result<()> {
    if yes {
        Err(MyError::new("crucio!"))
    } else {
        Ok(())
    }
}

fn parse_f64(s: &str, yes: bool) -> Result<f64> {
    gen_error(yes)?;
    let x: f64 = s.parse()?;
    Ok(x)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_custom_error() {
        assert!(gen_error(true).is_err());
        assert!(gen_error(false).is_ok());
    }

    #[test]
    fn test_convert_error() {
        let res = parse_f64("dsfkjhd", false);
        match res {
            Ok(_) => panic!("Shouldn't parse"),
            Err(x) => assert_eq!(x.to_string(), "invalid float literal"),
        }
    }

    #[test]
    fn test_ok_or_else() {
        // ok_or_else converts an Option into a Result
        let mut map = std::collections::HashMap::new();
        map.insert("no", 24);
        let val = map.get("key").ok_or_else(|| MyError::new("key not defined"));
        assert!(val.is_err());
        if let Err(x) = val {
            assert_eq!(x.to_string(), "key not defined");
        }
    }

}




