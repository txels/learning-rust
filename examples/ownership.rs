fn main() {
    {   
        let fs = "hello tomato";
        let mut s = String::from(fs);
        // s.push_str(" world"); // same as:
        s += " world";
        s.push_str(" of madness");
        // slices:
        let _hello = &s[..5];  // starting 0 is optional
        let _world = &s[7..12];
        let _all = &s[..];

        println!("{}", s);
        {
            let j = &mut s;
            println!("{}", j);
        }
        let fw = first_word_slice(&s);
        println!("fw = {}", fw);

        let fw = first_word_slice(fs);
        println!("fw = {}", fw);
    }
}



fn first_word_slice(s: &str) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[..i];
        }
    }

    &s[..]
}
