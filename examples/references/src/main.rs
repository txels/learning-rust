use std::fmt::Debug;

fn main() {
    // various smart pointers eamples
    box_example();
    rc_example();
}

fn box_example() {
    // recursive data structures with Box
    #[derive(Debug)]
    enum List {
        Cons(i32, Box<List>),
        Nil,
    }
    use List::{Cons, Nil};
    let list = Cons(1, Box::new(Cons(2, Box::new(Cons(3, Box::new(Nil))))));
    println!("{:?}", list);

    // referencing and de-referencing
    let x = 5;
    let y = &x;
    assert_eq!(5, x);
    assert_eq!(5, *y);
    assert_eq!(&5, y);

    // Box works like a reference (via the Deref trait)
    let y = Box::new(x);
    assert_eq!(5, *y);

    // -----------------------------------
    // replicating the Box behaviour
    use std::ops::Deref;

    // Struct is a tuple with one element that implements "Debug" (T:Debug):
    // (which we will need in order to use `{:?}` in `drop`
    #[derive(Debug)]
    struct MyBox<T: Debug>(T);

    impl<T: Debug> MyBox<T> {
        fn new(x: T) -> MyBox<T> {
            MyBox(x)
        }
    }

    impl<T: Debug> Deref for MyBox<T> {
        type Target = T;

        fn deref(&self) -> &Self::Target {
            &self.0
        }
    }

    impl<T: Debug> Drop for MyBox<T> {
        fn drop(&mut self) {
            println!("Dropping MyBox<T> with data `{:?}`!", self.0);
        }
    }

    let y = MyBox::new(x);
    assert_eq!(5, *y);
    println!("Explicit drop `{:?}`, will call `drop` on MyBox!", y);
    drop(y);

    fn hello(name: &str) {
        println!("Hello, {}!", name);
    }

    {
        let m = MyBox::new(String::from("Rust"));
        // automatically deref'd (coerced) into &str via &MyBox.deref()->&String.deref()->&str
        // (the compiler computes the deref chain)
        hello(&m);
        // equivalent code without deref coercion: manual deref `*` and slice `[..]`
        hello(&(*m)[..]);
    }

    println!("Drop 'Rust' should have happened");
    //println!("This was `moved` in drop(y) earler, won't compile... `{:?}`!", y);
}

fn rc_example() {
    use std::cell::RefCell;
    use std::rc::Rc;

    #[derive(Debug)]
    enum List {
        Cons(Rc<RefCell<i32>>, Rc<List>),
        Nil,
    }

    use List::{Cons, Nil};

    let value = Rc::new(RefCell::new(5));

    // Rc::clone creates a new reference to the value
    let a = Rc::new(Cons(Rc::clone(&value), Rc::new(Nil)));

    let b = Cons(Rc::new(RefCell::new(3)), Rc::clone(&a));
    let c = Cons(Rc::new(RefCell::new(4)), Rc::clone(&a));

    // now we can mutate values inside the data structure
    *value.borrow_mut() += 10;

    println!("a after = {:?}", a);
    println!("b after = {:?}", b);
    println!("c after = {:?}", c);

    if let Cons(_, y) = b {
        match &*y {
            Cons(x, _) => println!("(b's 2nd elem) = {:?}", x.borrow()),
            _ => (),
        }
    };
}
