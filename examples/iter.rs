fn main() {
    let arr = [1, 2, 4, 5, 6];
    let sum: i64 = arr.iter().sum();

    println!("{}", sum);
}
