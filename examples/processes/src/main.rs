use std::error::Error;
use std::io::Read;
use std::process::{Command, Stdio};

fn run() -> Result<(), Box<dyn Error>> {
    let mut cmd = Command::new("python");
    let out = cmd.arg("--version").output().expect("no python?");

    println!(
        "success={:?}\nout:\n{} ",
        out.status.success(),
        String::from_utf8(out.stdout)?.trim_end(),
    );

    Ok(())
}

fn spawn_waitout() -> Result<(), Box<dyn Error>> {
    let child = Command::new("rustc")
        .arg("-V")
        .stdout(Stdio::piped())
        .spawn()
        .expect("no rustc?");

    let res = child.wait_with_output()?;
    println!(
        "res {:?}\nout:\n{}\n",
        res.status,
        String::from_utf8(res.stdout)?.trim_end(),
    );

    Ok(())
}

fn spawn() -> Result<(), Box<dyn Error>> {
    let mut child = Command::new("rustc")
        .arg("-V")
        .stdout(Stdio::piped())
        .spawn()
        .expect("no rustc?");

    let res = child.wait()?;
    let mut out = String::new();
    child.stdout.unwrap().read_to_string(&mut out)?;
    println!(
        "res {:?}\nout:\n{}\n",
        res,
        out.trim_end(),
    );

    Ok(())
}

fn main() -> Result<(), Box<dyn Error>> {
    run()?;
    spawn()?;
    spawn_waitout()?;

    Ok(())
}
