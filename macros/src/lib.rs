#[macro_export]
macro_rules! cat {
    ( $( $x:expr ),* ) => {
        {
            let mut temp_vec = String::new();
            $(
                temp_vec.push_str($x);
                temp_vec.push_str(" ");
            )*
            temp_vec
        }
    };
}

macro_rules! s {
    ( $x:expr ) => {
        {
            String::from($x)
        }
    };
}

#[cfg(test)]
mod tests {
    #[test]
    fn s_macro() {
        let res = cat!("This", "is", "a", "list");
        assert_eq!("This is a list ", &res);

        let res = s!("This");
        assert_eq!("This", &res);
    }
}
