use std::fs;
use std::io::prelude::*;
use std::net::{TcpListener, TcpStream};
use std::thread;
use std::time::Duration;

use httpserver::terminator::Terminator;
use httpserver::ThreadPool;

fn main() {
    let terminator = Terminator::new();
    let listener = setup_tcp_listener();

    // Incoming requests will be processed by a thread pool
    // to limit number of parallel threads to use.
    let pool = ThreadPool::new(10);
    receive(pool, listener, terminator);
}

fn setup_tcp_listener() -> TcpListener {
    let listener = TcpListener::bind("127.0.0.1:7878").unwrap();
    // listener will not block on `incoming`.
    listener
        .set_nonblocking(true)
        .expect("Cannot set non-blocking");
    listener
}

/// Receive loop
fn receive(pool: ThreadPool, listener: TcpListener, terminator: Terminator) {
    for stream in listener.incoming() {
        if terminator.aborted() {
            break;
        }

        match stream {
            Ok(stream) => {
                pool.execute(|| {
                    handle_connection(stream);
                });
            }
            _ => {}
        }
    }
}

fn handle_connection(mut stream: TcpStream) {
    let mut buffer = [0; 1024];

    stream.read(&mut buffer).unwrap();
    let input = buffer.to_vec();
    let input = String::from_utf8(input).unwrap();
    let words: Vec<&str> = input.split_whitespace().collect();
    if words.len() >= 2 {
        if let [method, path, ..] = &words[..] {
            println!("Method={} Path={}", method, path);

            let (status, data, content_type) = if path == &"/" {
                (
                    "200 OK",
                    fs::read_to_string("index.html").unwrap(),
                    "text/html",
                )
            } else if path == &"/favicon.ico" {
                ("200 OK", String::new(), "text/plain")
            } else if path == &"/sleep" {
                thread::sleep(Duration::from_secs(5));
                ("200 OK", "Nighty night".to_string(), "text/plain")
            } else {
                (
                    "404 NOT FOUND",
                    "Nothing to see here.".to_string(),
                    "text/plain",
                )
            };
            let headers = format!(
                "{}\r\n\r\n",
                [
                    format!("Content-Type: {}; charset=utf-8", content_type),
                    format!("Content-Length: {}", data.len())
                ]
                .join("\r\n"),
            );
            send_response(&mut stream, status, &headers, &data);
        }
    } else {
        // println!("Fail to parse status_line: {}", input);
    }
}

fn send_response(stream: &mut TcpStream, status: &str, headers: &str, data: &str) {
    let status_line = format!("HTTP/1.1 {}\r\n", status);
    stream.write(status_line.as_bytes()).unwrap();
    stream.write(headers.as_bytes()).unwrap();
    stream.write(data.as_bytes()).unwrap();
    stream.flush().unwrap();
}
