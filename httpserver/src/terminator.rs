use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

pub struct Terminator {
    aborted: Arc<AtomicBool>,
}

impl Terminator {
    pub fn new() -> Self {
        let writer = Arc::new(AtomicBool::new(false));
        let reader = Arc::clone(&writer);

        ctrlc::set_handler(move || {
            println!("Received termination signal!");
            writer.store(true, Ordering::Relaxed);
        })
        .expect("Error setting Ctrl-C handler");
        Self { aborted: reader }
    }

    /// Use this to check whether execution was aborted
    pub fn aborted(&self) -> bool {
        self.aborted.load(Ordering::Relaxed)
    }
}

impl Clone for Terminator {
    /// Make a clone of this terminator, if you want to listen
    /// to termination in different places in your code
    fn clone(&self) -> Self {
        Self {
            aborted: Arc::clone(&self.aborted),
        }
    }
}
