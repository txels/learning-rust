# Notes from rusting

## Cool tools:

    rustup docs --book
    cargo doc --open
    cargo fix

Ecosystem:
[mdbook](https://rust-lang.github.io/mdBook/)

    cargo install mdbook
    mdbook 


## Language

tuple indexing: `t.0`, `t.1`...

`if` as an expression. Expressions don't end in ;

`{` starts a block... blocks are expressions.

String literal vs string variable: different memory allocation.
Strings use `"`, chars use `'` (like Java).

## Ownership etc...

Rust deallocates memory when owner (variable) goes out of scope. `drop`.

`move`: `let s2 = s1` does move the data from s1 to s2, and s1 is no longer valid.
`clone()`: (deep) copy the data.
`Copy` trait: types that implement it, data is copied not moved. This happens with
all stack-only data (e.g. primitive types like numbers, booleans...).

slices: a pointer and a length.

Slices:

    &[T] - shared slice
    &mut [T] - mutable slice
    Box<[T]> - owned slice


## Module system

crates, packages.

`mod` for modules. A crate is a tree of modules. Private by default, use `pub` for public.

Paths: `crate::` for root path (`/`), `super::` for parent (`../`), name for sibling (`./`).

General rule in Rust: private by default.

`use` works similarly to Python's `import`. `use ... as X` also works. `pub use` re-exports.
`pub use` enables having different internal vs public structure.

## Error handling

`Result` enum (`Ok(T)` and `Err(E)`)
Use with `match`, `unwrap_or_else`, `unwrap`, `expect`...

Auto-extract and propagate `?`: `expr?;`

## Generics, traits...

Traits ~== interfaces

## Lifetimes

Lifetime annotations help the compiler understand how long values last.

## Testing

Unit tests as a mod inside source file, with `#[cfg(test)]` annotation.

Test importable modules as `tests/<name>/mod.rs`.

## Iterators

Iterators implement `next()`, which returns an immutable reference to the next element.


## Publishing crates

[Publish to crates.io](file:///home/carles/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/share/doc/rust/html/book/ch14-02-publishing-to-crates-io.html)


## Internal mutability

Immutable structs that can modify their internal contents in a controlled
manner. Use `RefCell<T>` to wrap the internally mutable bits. Borrowing
checks are done at runtime.



## PENDING

- https://stevedonovan.github.io/rust-gentle-intro/6-error-handling.html#simple-error-for-simple-errors `simple-error` crate
- https://stevedonovan.github.io/rust-gentle-intro/6-error-handling.html#error-chain-for-serious-errors `error_chain` crate
- [Gitlab CI](file:///home/carles/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/share/doc/rust/html/cargo/guide/continuous-integration.html)
