use std::error::Error;
use cli_example::*;

pub fn main() {
    println!("Welcome to the Interactive Prompt! ");

    struct Data {
        answer: i32
    }

    let mut cli = Cli::new(Data{answer: 42});

    cli.cmd("count", "compute length of string", |data, args| {
        data.answer = args[0].chars().count() as i32;
        ok(data.answer)
    });

    cli.cmd("go", "set value", |data,args| {
        if args.len() == 0 { return err("need 1 argument"); }
        data.answer = match args[0].parse::<i32>() {
            Ok(n) => n,
            Err(e) => return err(e.to_string())
        };
        println!("got {:?}", args);
        ok(data.answer)
    });

    cli.cmd("show", "display value", |data,_| {
        ok(data.answer)
    });

    cli.go();
}
