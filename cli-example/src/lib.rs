use std::collections::HashMap;
use std::io;

pub type CliResult = Result<String, String>;

pub fn ok<T: ToString>(s: T) -> CliResult {
    Ok(s.to_string())
}

pub fn err<T: ToString>(s: T) -> CliResult {
    Err(s.to_string())
}

pub struct Cli<'a, D> {
    data: D,
    helplines: Vec<String>,
    callbacks: HashMap<String, Box<dyn Fn(&mut D, &[&str]) -> CliResult + 'a>>,
}

impl<'a, D: Sized> Cli<'a, D> {
    pub fn new(data: D) -> Cli<'a, D> {
        Cli {
            data: data,
            helplines: vec![],
            callbacks: HashMap::new(),
        }
    }

    pub fn cmd<F>(&mut self, name: &str, help: &str, callback: F)
    where
        F: Fn(&mut D, &[&str]) -> CliResult + 'a,
    {
        self.callbacks.insert(name.to_string(), Box::new(callback));
        self.helplines.push(format!("{}: {}", name, help));
    }

    fn process(&mut self, line: &str) -> CliResult {
        let parts: Vec<_> = line.split_whitespace().collect();
        if parts.len() == 0 {
            return Ok("".to_string());
        }
        if parts[0] == "help" {
            self.help()
        }
        else {
                match self.callbacks.get(parts[0]) {
                Some(callback) => callback(&mut self.data, &parts[1..]),
                None => Err("no such command".to_string()),
            }
        }
    }

    pub fn go(&mut self) {
        let mut buff = String::new();
        while io::stdin().read_line(&mut buff).expect("error") > 0 {
            {
                let line = buff.trim_start();
                let res = self.process(line);
                match res {
                    Ok(r) => println!("{}", r),
                    Err(r) => println!("ERROR: {}", r),
                }
            }
            buff.clear();
        }
    }
    
    pub fn help(&self) -> CliResult {
        let res: String = self.helplines.join("\n");
        ok(res)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn add_callbacks() {
        let data = String::from("hello");
        let mut cli = Cli::new(data);
        cli.cmd("count", "count length of next arg", |c, args| Ok(c.len().to_string()));
        assert_eq!(2 + 2, 4);
    }
}
