/// Using "json": low-level API
#[macro_use]
pub extern crate json;

/// Using "serde_json": [de]serialize JSON into rust structs
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

#[derive(Serialize, Deserialize, Debug)]
struct Person {
    name: String,
    age: u8,
    address: Address,
    phones: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug)]
struct Address {
    street: String,
    city: String,
}

/// regex
extern crate regex;
pub use regex::Regex;

/// chrono (datetime processing)
extern crate chrono;
// load chrono's traits
pub use chrono::prelude::*;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_json() {
        let mut doc = json::parse(
            r#"
    {
        "code": 200,
        "success": true,
        "payload": {
            "features": [
                "awesome",
                "easyAPI",
                "lowLearningCurve"
            ]
        }
    }
    "#,
        )
        .expect("parse failed");
        let code: u32 = doc["code"].as_u32().unwrap();
        assert_eq!(code, 200);
        assert_eq!(doc["code"], 200);
        assert_eq!(doc["success"], true);
        let features = &doc["payload"]["features"];
        assert_eq!(features[0], "awesome");
        let features = &mut doc["payload"]["features"];
        features.push("cargo!").expect("couldn't push");
        assert_eq!(features.len(), 4);
    }

    #[test]
    fn build_json() {
        let data = object! {
            "name"    => "John Doe",
            "age"     => 30,
            "numbers" => array![10,53,553]
        };
        assert_eq!(
            data.dump(),
            r#"{"name":"John Doe","age":30,"numbers":[10,53,553]}"#
        );
    }

    #[test]
    fn serde_parse() {
        let data = r#" {
            "name": "John Doe",
            "age": 43,
            "address": {"street": "main", "city":"Downtown"},
            "phones":["27726550023"]
        } "#;
        let p: Person = serde_json::from_str(data).expect("deserialize error");
        assert_eq!("John Doe", p.name);
        assert_eq!("27726550023", p.phones[0]);
    }

    #[test]
    fn regex1() {
        let re = Regex::new(r"(\d{2}):(\d+)").unwrap();
        assert_eq!("10", &re.captures("  10:230").unwrap()[1]);
        assert_eq!("230", &re.captures("  10:230").unwrap()[2]);
        assert_eq!("22", &re.captures("[22:2]").unwrap()[1]);
        assert!(re.captures("10:x23").is_none());
    }

    #[test]
    fn regex_named_captures() {
        let re = Regex::new(
            r"(?x)
(?P<year>\d{4})  # the year
-
(?P<month>\d{2}) # the month
-
(?P<day>\d{2})   # the day
",
        )
        .expect("bad regex");
        let caps = re.captures("2010-03-14").expect("match failed");

        assert_eq!("2010", &caps["year"]);
        assert_eq!("03", &caps["month"]);
        assert_eq!("14", &caps["day"]);
    }

    #[test]
    fn chrono_build_date() {
        const HOUR: i32 = 60 * 60;
        let date = chrono::FixedOffset::west(5 * HOUR)
            .ymd(2010, 3, 14)
            .and_hms(12, 20, 32);
        let localdate = chrono::Local.ymd(2010, 3, 14).and_hms(12, 20, 32);
        assert_eq!(2010, date.year());
        assert_eq!(12, date.hour());
        assert_eq!(Weekday::Sun, date.weekday());
        assert_eq!(-5 * HOUR, date.offset().fix().local_minus_utc());
        assert_eq!(1 * HOUR, localdate.offset().fix().local_minus_utc());
    }

    #[test]
    fn chrono_parse_dates() {
        let dt = Utc.ymd(2014, 11, 28).and_hms(12, 0, 9);
        let fixed_dt = dt.with_timezone(&FixedOffset::east(9 * 3600));

        // method 1
        assert_eq!(
            "2014-11-28T12:00:09Z".parse::<DateTime<Utc>>(),
            Ok(dt.clone())
        );
        assert_eq!(
            "2014-11-28T21:00:09+09:00".parse::<DateTime<Utc>>(),
            Ok(dt.clone())
        );
        assert_eq!(
            "2014-11-28T21:00:09+09:00".parse::<DateTime<FixedOffset>>(),
            Ok(fixed_dt.clone())
        );
    }
}
