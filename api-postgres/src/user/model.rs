use postgres::{Client, NoTls};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct User {
    pub id: i32,
    pub email: String,
}

impl User {
    pub fn connect(user: &str) -> Client {
        let connect_str = format!("host=localhost user={}", user);
        Client::connect(&connect_str, NoTls).unwrap()
    }

    pub fn all() -> Vec<User> {
        let mut users: Vec<User> = Vec::new();
        let mut client = Self::connect("postgres");

        if let Ok(results) = client.query("SELECT id, email from accounts_customuser", &[]) {
            for row in results {
                let id: i32 = row.get(0);
                let email: String = row.get::<_, &str>(1).to_string();
                users.push(User { id, email });
            }
        }
        users
    }

    pub fn get(id: i32) -> Option<User> {
        let mut client = Self::connect("postgres");
        let result = client.query(
            "SELECT id, email from accounts_customuser where id = $1",
            &[&id],
        );
        if let Ok(ref rows) = result {
            // println!("Rows={:#?}", rows);
            if let Some(row) = rows.get(0) {
                // println!("Row={:?}", row);
                let id: i32 = row.get(0);
                let email: String = row.get::<_, &str>(1).to_string();
                return Some(User { id, email });
            }
        }
        println!("Not found. Query returned {:?}", result);
        None
    }
}
