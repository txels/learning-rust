use actix_web::{get, web, HttpResponse, Responder};

use crate::user::User;

pub fn init_routes(cfg: &mut web::ServiceConfig) {
    cfg.service(all);
    cfg.service(get);
}

#[get("/users")]
async fn all() -> impl Responder {
    HttpResponse::Ok().json(User::all())
}

#[get("/users/{id}")]
async fn get(id: web::Path<i32>) -> impl Responder {
    let id = id.into_inner();
    println!("/users/id={:?}", &id);
    HttpResponse::Ok().json(User::get(id))
}
