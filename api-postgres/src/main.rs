use std::env;
use std::io;

use actix_web::{get, App, HttpResponse, HttpServer, Responder};
// listen for hot reloading
use listenfd::ListenFd;

mod user;

fn main() {
    let rhex = rand_password();
    println!("{}", rhex);
    let args: Vec<String> = env::args().skip(1).collect();
    if args.len() == 2 {
        let user = &args[0];
        create_db(user, &args[1]);
        psql(user);
    } else {
        psql("postgres");
    }
}

#[actix_rt::main]
async fn server() -> std::io::Result<()> {
    let listen_on = "127.0.0.1:5000";
    println!("Starting HTTP server on http://{}", listen_on);

    let mut listenfd = ListenFd::from_env();

    let mut server = HttpServer::new(|| App::new().service(index).configure(user::init_routes));

    server = match listenfd.take_tcp_listener(0)? {
        Some(listener) => server.listen(listener)?,
        None => server.bind(listen_on)?,
    };

    server.run().await
}

#[get("/")]
async fn index() -> impl Responder {
    HttpResponse::Ok().body("Hello world!")
}

fn psql(user: &str) {
    let mut client = user::User::connect(user);

    println!("Query: ");
    let mut line = String::new();

    loop {
        io::stdin()
            .read_line(&mut line)
            .expect("Failed to read line");
        let query = line.trim_end();
        if query == "exit" {
            break;
        }
        let result = client.execute(query, &[]);
        println!("Query returned {:?}", result);
        line.clear();
    }
}

fn create_db(user: &str, password: &str) {
    let mut client = user::User::connect("postgres");
    let queries = [
        format!(
            "create user {user} password '{password}'",
            user = user,
            password = password
        ),
        format!("create database {user} owner {user}", user = user),
    ];
    for query in &queries {
        let result = client.execute(&query[..], &[]);
        println!("Query returned {:?}", result);
    }
}

fn rand_password() -> String {
    let rand: [u8; 24] = rand::random();
    let rhex: String = rand.iter().map(|x| {format!("{:x}", x)}).collect();
    rhex
}
