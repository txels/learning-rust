use kube::CustomResource;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(CustomResource, Serialize, Deserialize, Debug, PartialEq, Clone, JsonSchema)]
#[kube(
    group = "k8s.txels.com",
    version = "v1",
    kind = "Database",
    plural = "databases",
    shortname = "db",
    derive = "PartialEq",
    namespaced
)]
pub struct DatabaseSpec {
    pub username: Option<String>,
}

#[cfg(test)]
mod test {
    use super::*;
    // use trait for CRD name()
    use kube::ResourceExt;

    #[test]
    fn custom_resource_creation() {
        let res = Database::new(
            "db",
            DatabaseSpec {
                username: Some("user".to_owned()),
            },
        );
        assert_eq!(res.name(), "db");
        assert_eq!(res.kind, "Database");
    }

    #[test]
    fn custom_resource_equality() {
        let res = Database::new(
            "db",
            DatabaseSpec {
                username: Some("user".to_owned()),
            },
        );
        let res_equal = Database::new(
            "db",
            DatabaseSpec {
                username: Some("user".to_owned()),
            },
        );
        let res_different = Database::new(
            "db",
            DatabaseSpec {
                username: Some("other".to_owned()),
            },
        );
        assert_eq!(res, res_equal);
        assert_ne!(res, res_different);
    }

    #[test]
    fn custom_resource_clone() {
        let res = Database::new(
            "db",
            DatabaseSpec {
                username: Some("user".to_owned()),
            },
        );
        let res_clone = res.clone();
        assert_eq!(res, res_clone);
    }

    #[test]
    fn custom_resource_json() {
        let res = Database::new(
            "db",
            DatabaseSpec {
                username: Some("user".to_owned()),
            },
        );
        let res_json = serde_json::to_string(&res);
        assert_eq!(res_json.unwrap(), "{\"apiVersion\":\"k8s.txels.com/v1\",\"kind\":\"Database\",\"metadata\":{\"name\":\"db\"},\"spec\":{\"username\":\"user\"}}");
    }
}
