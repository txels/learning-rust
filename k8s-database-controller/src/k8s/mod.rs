use k8s_openapi::api::core::v1::Secret;
use kube::api::{Api, ListParams};
use kube::Client;

pub mod controller;
pub mod crd;
pub mod finalizer;

pub use crd::Database;

pub async fn list_databases(client: Client, namespace: &str) -> Result<(), kube::Error> {
    let db_api: Api<Database> = Api::namespaced(client, namespace);
    let lp = ListParams::default();
    let all_dbs = db_api.list(&lp).await?;

    for db in all_dbs {
        let name = match &db.metadata.name {
            Some(n) => n,
            None => "Not found",
        };

        let username = match &db.spec.username {
            Some(n) => n,
            None => "Not found",
        };

        println!("Got database: name={:?} username={:?}", name, username);
    }
    Ok(())
}

pub async fn list_secrets(client: Client, namespace: &str) -> Result<(), kube::Error> {
    let secret_api: Api<Secret> = Api::namespaced(client, namespace);
    let lp = ListParams::default();
    let all_secrets = secret_api.list(&lp).await?;

    for secret in all_secrets {
        let name = match &secret.metadata.name {
            Some(n) => n,
            None => "Not found",
        };

        println!("Got secret: {:?}", name);
    }
    Ok(())
}
