use std::sync::Arc;
use tokio_postgres::{Client, Error, NoTls};
use log::*;

#[derive(Debug, Clone)]
pub struct DBServer {
    client: Arc<Client>,
}

impl DBServer {
    pub(crate) async fn new(url: &str) -> Result<Self, Error> {
        let (client, connection) = tokio_postgres::connect(&url, NoTls).await?;

        tokio::spawn(async move {
            if let Err(e) = connection.await {
                error!("connection error: {}", e);
            }
        });

        Ok(Self {
            client: Arc::new(client),
        })
    }

    async fn execute(&self, queries: &Vec<String>) -> Result<(), Error> {
        for query in queries {
            let result = self.client.execute(&query[..], &[]).await?;
            // TODO: error handling - properly propagate DB creation error
            println!("Query '{}'\nReturned {:?}", query, result);
        }
        Ok(())
    }

    pub async fn create_database(&self, user: &str, password: &str) -> Result<(), Error> {
        let queries = vec![
            format!(
                "create user {user} password '{password}'",
                user = user,
                password = password
            ),
            format!("create database {user} owner {user}", user = user),
        ];
        self.execute(&queries).await
    }

    pub async fn drop_database(&self, user: &str) -> Result<(), Error> {
        let queries = vec![
            format!("drop database {user}", user = user),
            format!("drop user {user}", user = user),
        ];
        self.execute(&queries).await
    }

    pub async fn disable_user(&self, user: &str) -> Result<(), Error> {
        let queries = vec![format!("alter user {user} with nologin", user = user)];
        self.execute(&queries).await
    }
}
