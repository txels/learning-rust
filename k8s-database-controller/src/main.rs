use std::env;
use std::error::Error;

pub mod database;
pub mod k8s;
pub mod random;
//use crate::k8s;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    println!("Started controller.");
    let dbhost = env::var("DB_HOST").unwrap_or_else(|_| "localhost".to_owned());
    let dbuser = env::var("DB_USER").unwrap_or_else(|_| "postgres".to_owned());
    let connect_str = format!("host={} user={}", dbhost, dbuser);
    let dbserver = database::DBServer::new(&connect_str).await?;
    k8s::controller::run(dbserver).await;
    Ok(())
}
